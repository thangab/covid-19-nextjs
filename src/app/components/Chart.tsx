import React from 'react';
import {
  ResponsiveContainer,
  AreaChart,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Area,
  Legend,
} from 'recharts';
import moment from 'moment';

export default ({ data }) => {
  const chartData = data
    .map(d => ({ ...d, Date: moment(d.Date).format('LL') }))
    .filter(d => d.Confirmed > 0 || d.Deaths > 0 || d.Recovered > 0);
  return (
    <ResponsiveContainer height={400}>
      <AreaChart data={chartData}>
        <defs>
          <linearGradient id="colorConfirmed" x1="0" y1="0" x2="0" y2="1">
            <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8} />
            <stop offset="95%" stopColor="#8884d8" stopOpacity={0} />
          </linearGradient>
          <linearGradient id="colorRecovered" x1="0" y1="0" x2="0" y2="1">
            <stop offset="5%" stopColor="#82ca9d" stopOpacity={0.8} />
            <stop offset="95%" stopColor="#82ca9d" stopOpacity={0} />
          </linearGradient>
          <linearGradient id="colorDeaths" x1="0" y1="0" x2="0" y2="1">
            <stop offset="5%" stopColor="#333333" stopOpacity={0.8} />
            <stop offset="95%" stopColor="#333333" stopOpacity={0} />
          </linearGradient>
        </defs>
        <XAxis dataKey="Date" />
        <YAxis />
        <CartesianGrid strokeDasharray="3 3" />
        <Tooltip />
        <Area
          type="monotone"
          dataKey="Confirmed"
          stroke="#8884d8"
          fillOpacity={1}
          fill="url(#colorConfirmed)"
        />
        <Area
          type="monotone"
          dataKey="Recovered"
          stroke="#82ca9d"
          fillOpacity={1}
          fill="url(#colorRecovered)"
        />
        <Area
          type="monotone"
          dataKey="Deaths"
          stroke="#333333"
          fillOpacity={1}
          fill="url(#colorDeaths)"
        />
        <Legend />
      </AreaChart>
    </ResponsiveContainer>
  );
};
