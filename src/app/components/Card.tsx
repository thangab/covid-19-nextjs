import React from 'react';
import moment from 'moment';

interface IProps {
  icon: React.ReactNode | string;
  title: string;
  date?: Date;
}

const Card: React.FC<IProps> = ({ icon, title, date, children }) => (
  <div className="flex items-center m-6 p-6 bg-white shadow-md rounded">
    <div className="flex-shrink-0">
      <div className="flex items-center justify-center h-12 w-12 rounded-md bg-indigo-500 text-white font-semibold text-xl">
        {icon}
      </div>
    </div>
    <div className="ml-4 w-full">
      <h2 className="text-lg leading-6 font-medium text-gray-900 uppercase">{title}</h2>
      {date && <p className="text-sm text-gray-600 flex items-center">{moment(date).fromNow()}</p>}
      <div className="mt-2 text-base leading-6">{children}</div>
    </div>
  </div>
);

export default Card;
