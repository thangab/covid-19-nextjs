import App from 'components/App';
import Error from 'next/error';
import fetch from 'isomorphic-unfetch';
import { NextPageContext } from 'next';
import { ICountryDetail, IError } from 'components/types';
import Chart from 'components/Chart';
import Button from 'components/Button';

interface IProps {
  data: ICountryDetail[];
  error?: IError;
}

const Country = ({ data, error }: IProps) => {
  if (error || !Array.isArray(data)) return <Error statusCode={500} />;

  const [firstData] = data;

  return (
    <App>
      <Button path="/">BACK</Button>
      <h1>Country: {firstData.Country}</h1>
      <h5>Covid-19 Stats per day since the begining of the pandemic</h5>
      <div className="container p-6">
        <Chart data={data} />
      </div>
    </App>
  );
};

Country.getInitialProps = async (ctx: NextPageContext) => {
  try {
    const { slug } = ctx.query;
    const res = await fetch(`https://api.covid19api.com/total/country/${slug}`);
    const json = await res.json();
    return { data: json };
  } catch (error) {
    return { error };
  }
};

export default Country;
