import { NextPage } from 'next';
import Error from 'next/error';
import App from 'components/App';
import fetch from 'isomorphic-unfetch';
import { IError, IGlobal, ICountry } from 'components/types';
import Card from 'components/Card';
import moment from 'moment';
import CountUp from 'react-countup';
import Button from 'components/Button';

interface IProps {
  data?: {
    Global?: IGlobal;
    Countries?: ICountry[];
    message?: string;
  };
  error?: IError;
}

const Home: NextPage<IProps> = ({ data, error }) => {
  if (error || data?.message) return <Error statusCode={500} />;

  const compareCountryByKey = countryKey => (countryA, countryB) =>
    countryA[countryKey] < countryB[countryKey] ? 1 : -1;
  const countriesFiltered =
    data?.Countries &&
    [...data?.Countries].sort(compareCountryByKey('TotalConfirmed')).slice(0, 10);

  return (
    <App>
      <h1 className="px-6">CORONA VIRUS (COVID-19) CASES ON {moment().format('MMMM Do YYYY')}</h1>
      <div className="md:grid md:grid-cols-2 md:col-gap-8 md:row-gap-10">
        <Card
          icon={
            <svg className="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9"
              />
            </svg>
          }
          title="Wolrd total"
        >
          <div>
            <p>
              Confirmed: <CountUp end={data?.Global?.TotalConfirmed || 0} />
            </p>
            <p>
              Deaths: <CountUp end={data?.Global?.TotalDeaths || 0} />
            </p>
            <p>
              Recovered: <CountUp end={data?.Global?.TotalRecovered || 0} />
            </p>
          </div>
        </Card>
        <Card
          icon={
            <svg className="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M13 10V3L4 14h7v7l9-11h-7z"
              />
            </svg>
          }
          title="New Cases"
        >
          <div>
            <p>
              Confirmed: <CountUp end={data?.Global?.NewConfirmed || 0} />
            </p>
            <p>
              Deaths: <CountUp end={data?.Global?.NewDeaths || 0} />
            </p>
            <p>
              Recovered: <CountUp end={data?.Global?.NewRecovered || 0} />
            </p>
          </div>
        </Card>
      </div>
      {/* Top ten most-affected countries */}
      <h2 className="p-6">Top ten most-affected countries</h2>
      <div className="md:grid md:grid-cols-2 md:col-gap-8 md:row-gap-10">
        {countriesFiltered?.map((country, index) => {
          return (
            <Card
              key={country.Slug + index}
              icon={country.CountryCode}
              title={country.Country}
              date={country.Date}
            >
              <div className="flex justify-around">
                <div className="mr-6">
                  <h3 className="underline">Total</h3>
                  <ul>
                    <li>Confirmed: {country.TotalConfirmed}</li>
                    <li>Deaths: {country.TotalDeaths}</li>
                    <li>Recovered: {country.NewRecovered}</li>
                  </ul>
                </div>
                <div className="mr-6">
                  <h3 className="underline">New</h3>
                  <ul>
                    <li>Confirmed: {country.NewConfirmed}</li>
                    <li>Deaths: {country.NewDeaths}</li>
                    <li>Recovered: {country.NewRecovered}</li>
                  </ul>
                </div>
              </div>
              <div className="flex justify-center">
                <Button path={`/country/${country.Slug}`}>See more details</Button>
              </div>
            </Card>
          );
        })}
      </div>
    </App>
  );
};

Home.getInitialProps = async () => {
  try {
    const res = await fetch('https://api.covid19api.com/summary');
    const json = await res.json();
    return { data: json };
  } catch (error) {
    return { error };
  }
};

export default Home;
