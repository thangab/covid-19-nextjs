import App from 'components/App';

export default () => (
  <App>
    <div className="text-center">
      <p>Made by Thang</p>
      <p>
        {'Data is provided by '}
        <a href="https://www.covid19api.com/" target="_blank">
          https://www.covid19api.com/
        </a>
      </p>
    </div>
  </App>
);
