import NavBar from './NavBar';

export default () => (
  <header>
    <NavBar />
  </header>
);
