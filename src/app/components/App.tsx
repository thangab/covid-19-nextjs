import Header from './Header';

const App = ({ children }: { children?: any }) => (
  <div className="bg-gray-100 antialiased">
    <Header />
    <div className="w-full p-6">
      <div className="mt-6 sm:mt-0 sm:py-12">{children}</div>
    </div>
  </div>
);

export default App;
